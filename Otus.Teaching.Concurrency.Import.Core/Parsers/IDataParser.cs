﻿namespace Otus.Teaching.Concurrency.Import.Core.Parsers
{
    public interface IDataParser<T>
    {
        T Parse(string str);
    }

    public interface IXmlDataParser<T> : IDataParser<T> { }
    public interface ICSVDataParser<T> : IDataParser<T> { }
}
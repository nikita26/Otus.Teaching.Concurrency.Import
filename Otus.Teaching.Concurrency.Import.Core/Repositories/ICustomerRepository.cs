using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);
        void AddListCustomers(List<Customer> customers);
        IEnumerable<Customer> GetAll();
        void SaveChangesAsync();
    }
}
﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "";
            int consumersCount = 0;
            int setLenght = 0;


            var hostBuilder = Host.CreateDefaultBuilder();
            hostBuilder.
            ConfigureServices((context,services) =>
            {
                services.AddSingleton<IApp, App>();
                services.AddTransient<ICustomerRepository, CustomerRepository>();
                services.AddTransient<IDataLoader, DataLoader>();

                //Выбор генератора
                //services.AddTransient<IDataGenerator, DataGenerator.Generators.XmlGenerator>();
                services.AddTransient<IDataGenerator, DataGenerator.Generators.CSVGeneretor>();

                services.AddTransient<ICSVDataParser<CustomersList>, CSVParser>();
                services.AddTransient<IXmlDataParser<CustomersList>, XmlParser>();

                services.AddDbContext<DataBaseContext>(options =>
                {
                    options.UseNpgsql(context.Configuration.GetConnectionString("DefaultConnection"));
                },
                ServiceLifetime.Transient);


                fileName = context.Configuration.GetSection("FileName").Value;
                consumersCount = int.Parse(context.Configuration.GetSection("ConsumersCount").Value);
                setLenght = int.Parse(context.Configuration.GetSection("SetLenght").Value);
            }).
            ConfigureLogging(o =>
            {
                o.SetMinimumLevel(LogLevel.None);
            });

            var host = hostBuilder.Build();

            var app = host.Services.GetService<IApp>();
            app.Run(fileName,consumersCount,setLenght);
        }
    }
}
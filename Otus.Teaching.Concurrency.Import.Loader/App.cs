﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    internal interface IApp
    {
        void Run(string fileName, int consumersCount, int setLenght);
    }

    internal class App : IApp
    {
        private string _dataFilePath;
        private int _consumersCount; //количество объектов которые нужно сгенерировать
        private int _setLenght; //количество объектов в одной транзакции
        
        private object lockConsole = new object();

        private readonly IDataLoader _dataLoader;
        private readonly IDataGenerator _dataGenerator;
        private readonly IXmlDataParser<CustomersList> _xmlParser;
        private readonly ICSVDataParser<CustomersList> _csvParser;
        public App(IDataLoader dataLoader,IDataGenerator dataGenerator,
            IXmlDataParser<CustomersList> xmlParser, ICSVDataParser<CustomersList> csvParser)
        {
            _dataLoader = dataLoader;
            _dataGenerator = dataGenerator;

            _xmlParser = xmlParser;
            _csvParser = csvParser;
        }

        public void Run(string fileName,int consumersCount,int setLenght) {


            _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);
            _consumersCount = consumersCount;
            _setLenght = setLenght;

            GenerateCustomersDataFile();

            CustomersList customers = null;

            var str = File.ReadAllText(_dataFilePath);
            try
            {
                customers = _xmlParser.Parse(str);
            }
            catch(Exception ex)
            {
                customers = _csvParser.Parse(str);
            }

            Console.WriteLine("Start writing to database ...");

            var sw = new Stopwatch();
            sw.Start();
            var totalThread = 0;
            var thread = 0;
            var me = new ManualResetEvent(false);

            if (customers.Customers.Count <= _setLenght)
            {
                thread = 1;
                totalThread = thread;
                ThreadPool.QueueUserWorkItem(c =>
                {
                    _dataLoader.LoadData(customers.Customers);
                    if (Interlocked.Decrement(ref thread) == 0)
                        me.Set();
                });
            }
            else
            {
                Console.Write("Progress: 0%");
                var positon = Console.GetCursorPosition();

                int setCount = customers.Customers.Count / _setLenght;
                thread = setCount;
                totalThread = thread;

                int lastSetLenght = customers.Customers.Count - (setCount * _setLenght);
                if(lastSetLenght > 0) {
                    thread++;
                    totalThread = thread;

                    var lastSet = customers.Customers.GetRange(setCount * _setLenght, lastSetLenght);

                    ThreadPool.QueueUserWorkItem(c => {
                        _dataLoader.LoadData(lastSet);
                        if(Interlocked.Decrement(ref thread) == 0)
                            me.Set();
                    });
                }

                for (int i = 0; i <= customers.Customers.Count - _setLenght; i+= _setLenght)
                {
                    var set = customers.Customers.GetRange(i, _setLenght);
                    ThreadPool.QueueUserWorkItem(c => {
                        _dataLoader.LoadData(set);
                        if (Interlocked.Decrement(ref thread) == 0)
                            me.Set();

                        lock(lockConsole)
                        {
                            Console.SetCursorPosition(0, positon.Top);
                            Console.Write($"Progress: {100 - (thread * 100 / (setCount))}%");
                        }
                    });
                }
            }

            //Ожидание завершения всех потоков
            me.WaitOne();
            sw.Stop();
            float time = sw.ElapsedMilliseconds / 1000f;
            Console.WriteLine($"\n{time} seconds, {totalThread} threads");
            Console.ReadKey();
        }
        
        /// <summary>
        /// Генерация исходного файла
        /// </summary>
        private void GenerateCustomersDataFile()
        {
            Console.WriteLine("1 - Генерация методом, 2 - Генерация через процесс");
            var q = Console.ReadLine();
            if (q == "1")
            {
                Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");
                _dataGenerator.Generate(_dataFilePath, _consumersCount);
            }
            else
            {
                var info = new ProcessStartInfo(AppDomain.CurrentDomain.BaseDirectory + "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe", $"customers {_consumersCount}");
                var process = Process.Start(info);
                Console.WriteLine($"Loader started with process Id {process.Id}...");
                process.WaitForExit();
            }
        }
    }
}

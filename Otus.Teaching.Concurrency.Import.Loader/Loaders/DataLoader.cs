﻿using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class DataLoader : IDataLoader
    {
        private readonly IServiceProvider _provider;

        public DataLoader(IServiceProvider provider) 
        {
            _provider = provider;
        }
        public void LoadData(List<Customer> customers)
        {
            var rep = _provider.GetService<ICustomerRepository>();
            rep.AddListCustomers(customers);
        }
    }
}

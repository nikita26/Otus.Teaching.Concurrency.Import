﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CSVParser
        : ICSVDataParser<CustomersList>
    {
        public CustomersList Parse(string csv)
        {
            var customerList = new CustomersList() { Customers = new List<Customer>()};
            var items = csv.Split("\n");
            foreach(var item in items)
            {
                if(item != string.Empty)
                {
                    var fields = item.Split(',');
                    customerList.Customers.Add(new Customer()
                    {
                        Id = int.Parse(fields[0]),
                        FullName = fields[1],
                        Email = fields[2],
                        Phone = fields[3],
                    });
                }
            }
            return customerList;
        }
    }
}
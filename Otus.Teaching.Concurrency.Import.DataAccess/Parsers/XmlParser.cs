﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IXmlDataParser<CustomersList>
    {
        public CustomersList Parse(string xml)
        {
            using TextReader reader = new StringReader(xml);
            var s = new XmlSerializer(typeof(CustomersList));
            var list = (CustomersList)s.Deserialize(reader);
            return list;
        }
    }
}
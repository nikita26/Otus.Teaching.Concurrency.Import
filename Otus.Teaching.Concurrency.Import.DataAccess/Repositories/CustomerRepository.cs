using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        private readonly DataBaseContext _dataBaseContext;
        private readonly DbSet<Customer> _customers;
        public CustomerRepository(DataBaseContext dataBaseContext)
        {
            _dataBaseContext = dataBaseContext;
            _customers = _dataBaseContext.Set<Customer>();
        }
        public void AddCustomer(Customer customer)
        {
            _customers.Add(customer);
        }
        public void AddListCustomers(List<Customer> customers)
        {
            using var t = _dataBaseContext.Database.BeginTransaction();
            try
            {
                foreach (var item in customers)
                    _customers.Add(item);
                _dataBaseContext.SaveChanges();
                t.Commit();
            }
            catch (Exception ex)
            {
                t.Rollback();
            }
        }

        public IEnumerable<Customer> GetAll()
        {
            return _customers.Select(c => c).ToList();
        }
        public async void SaveChangesAsync()
        {
            await _dataBaseContext.SaveChangesAsync();
        }
    }
}
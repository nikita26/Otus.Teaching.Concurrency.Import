﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using System.IO;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CSVGeneretor : IDataGenerator
    {
        public void Generate(string fileName, int dataCount)
        {
            var customers = RandomCustomerGenerator.Generate(dataCount);
            using var file = File.Create(fileName);
            using var writer = new StreamWriter(file);
            string data = "";
            foreach(var c in customers)
            {
                writer.Write($"{c.Id},{c.FullName},{c.Email},{c.Phone}\n");
            }
                //data += $"{c.Id},{c.FullName},{c.Email},{c.Phone}\n";
            //File.WriteAllText(fileName, data);
        }
    }
}
